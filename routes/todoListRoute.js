/**
 * Module dependencies.
 */
var express = require('express');
var router = express.Router(); // Utilice la clase express.Router para crear manejadores de rutas montables y modulares. Una instancia Router es un sistema de middleware y direccionamiento completo
var todoList = require('../controllers/todoListController')


// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});


// define the home page route
router.get('/', function (req, res) {
    res.send('Welcome to the TodoList home page');
});


/*todoList*/
// router.get('/addNewItem', todoList.list);
// router.get('/getAll', todoList.list);


module.exports = router;